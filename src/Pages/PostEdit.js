import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { Post } from "../Util/JSONUtils";

function PostEdit() {
  const [title, setTitle] = useState("");
  const [discription, setDiscription] = useState("");
const {push} = useHistory()

  const onSubmitHandler = (event) => {
    event.preventDefault();
    // alert();
    Post("http://localhost:3002/posts", { title, discription })
      .then(() => {
          setTitle("");
          setDiscription();
         push("/post");
      })
      .catch((err) => {
          console.log(err)
      });
  };
  return (
    <div>
      <form onSubmit={onSubmitHandler}>
        <div>
          <label>Title</label>
          <input
            type="text"
            value={title}
            onChange={(event) => {
              setTitle(event.target.value);
            }}
          />
        </div>
        <div>
          <label>Descrition</label>
          <input
            type="text"
            value={discription}
            onChange={(event) => {
              setDiscription(event.target.value);
            }}
          />
        </div>
        <div>
          <button type="submit">Submit</button>
        </div>
      </form>
    </div>
  );
}

export default PostEdit;
