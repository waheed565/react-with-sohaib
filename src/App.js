import React,{useState} from "react";
import "./App.css";
import { Button, ButtonGroup, list, ListItem } from "@chakra-ui/react";
import { Switch, Route } from "react-router-dom";
import PostEdit from "./Pages/PostEdit";

const useCounter = () =>{
  const [count, setCount] = useState(3);
  return{
    count,
    addInc: () => {
      setCount(count+1)
    },
    decInc: () => {
      setCount(count-1)
    }
  }
}

const useList = () => {
  const [value, setValue ] = useState([]);
  return {
    list: value,
    push: (newList) => {
setValue([...value, newList])
    },
    pull: (index)=>{
const newList = value.filter((v, newIndex) =>{
  return index !== newIndex;
})
setValue(newList)
    }
  }
}
function App() {
  console.log("helo", useCounter);
  const { count, addInc, decInc } = useCounter();
  const {list, push, pull } = useList()
  return (
    <div className="App">
    {count}
    <button onClick={addInc}>Add</button>
    <button onClick={decInc}>Dec</button>
    <ul>
      {list.map((listItem, index) => {
        return <li onClick={() => pull(index)} key={index}>{listItem}</li>
      })}
    </ul>
    <button onClick={() => push("hello world")}>Add List</button>
      {/* <Switch>
        <Route exact path="/">what</Route>
        <Route exact path="/post/new"><PostEdit/></Route>
        <Route exact path="/post">kya</Route>
        <Route path="/post/:id">osmej</Route>

      </Switch> */}
    </div>
  );
}

export default App;
